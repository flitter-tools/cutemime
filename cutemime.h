#ifndef CUTEMIME_H
#define CUTEMIME_H

#include <QString>
#include <QIcon>
#include <QStringList>
#include <QMap>

struct DesktopAction
{
    QString actionName;
    QString name;
    QString exec;
};

struct Desktop
{
    QString name;
    QString genericName;
    QString exec;
    QString tryExec;
    QIcon icon;
    QString iconRealName;
    QStringList categories;
    QStringList actions;
    QString comment;
    bool terminal;
    bool noDisplay;
    QStringList mimeType;
    //QList<DesktopAction> actionsList; // stub

    QString filePath;
};

class CuteMime
{
    public:
        CuteMime(){}
        ~CuteMime(){}

        QMap<QString, QStringList> getMimeList();
        Desktop parseDesktopFile(const QString &filePath, QString region = "");
        Desktop findDesktopFile(const QString &file);
        QList<Desktop> getListDesktopFiles(const QString &region = "", bool skipNodisplay = true);
        void write(Desktop desktop);

    private:
        QIcon getHicolorIcon(const QString &iconName);
        QIcon getThemeIcon(QString iconName);
};

#endif // CUTEMIME_H
